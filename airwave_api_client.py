import json
import requests
import xml.etree.ElementTree as ET
#not until 3.8 :cry:
#from functools import cached_property


class AirWaveAPIClient(object):
    def __init__(self,host,credentials='credentials.json'):
        self.host = host
        self.credentials_file = credentials
        self.session = requests.Session()
        self._authenticate()
        self._amp_stats()

        for stat in ('client_count', 'bandwidth_in', 'bandwidth_out',
                     'down', 'down_wired', 'down_wireless',
                     'up', 'up_wired', 'up_wireless',
                     'alerts', 'severe_alerts',
                     'rogue', 'audit_disabled', 'mismatched', 'new_count', 'configuration_unknown',
                     'vpn_all_users', 'vpn_bandwidth_in', 'vpn_bandwidth_out', 'vpn_count') :
            node = self.amp_stats.find(stat)
            try:
                value = int(node.text)
            except AttributeError:
                setattr(self,stat,None)
            setattr(self,stat,value)

    def _read_credentials(self):
        with open(self.credentials_file) as f:
            self.credentials = json.load(f)

    def _authenticate(self):
        self._read_credentials()
        auth_params = {'credential_0': self.credentials['username'],
                       'credential_1': self.credentials['password'],
                       'destination': '/',
                       'login':'Log In'}
        r = self.session.post(f'https://{self.host}/LOGIN', data=auth_params)

    def _amp_stats(self):
        r = self.session.get(f'https://{self.host}/amp_stats.xml', params={'include_bandwidth':1})
        xml = r.content
        self.amp_stats = ET.fromstring(xml)


if __name__ == '__main__':
    host = 'airwave.nms.csusb.edu'
    airwave = AirWaveAPIClient(host)
    count =  airwave.client_count
    print(f"{count:10d} clients")
    print(f"{airwave.severe_alerts:10d} severe alerts")
    print(f"{airwave.alerts:10d} alerts")
    print(f"{airwave.up_wireless:10d} APs up")
    print(f"{airwave.down_wireless:10d} APs down")
    print(f"{airwave.bandwidth_in / 1024 / 1024:10.02f} Mbps In")
    print(f"{airwave.bandwidth_out / 1024 / 1024:10.02f} Mbps Out")
