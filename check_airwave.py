import logging
import sys
import json
from airwave_api_client import AirWaveAPIClient

class Nagios():
  OK = 0
  WARN = 1
  CRITICAL = 2

def main():
    config = json.load(open('config.json'))
    logging.debug(f'{config}')
    airwave = AirWaveAPIClient(config['host'])
    stats = [ 'client_count', 'up_wireless' ]
    for stat in stats:
        low = config[stat][0]
        high = config[stat][1]
        value = getattr(airwave,stat)
        if low <= value <= high:
            continue
        print(f'WARN {stat}={value} and threshold: {low} <= {stat} <= {high})')
        sys.exit(Nagios.WARN)
    print(f'OK checked {len(stats)} stats')
    sys.exit(Nagios.OK)

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    main()
